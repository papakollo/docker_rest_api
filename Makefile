#Makefile
#include env Variables
include .env
export

# Scope variables
CDC=docker-compose
CURRENT_DIR=${PARENT_FOLDER}/infra
DATA_PATH=${CURRENT_DIR}/data
LOG_APACHE_DIR=${CURRENT_DIR}/log/apache

#Recreate containers even if their configuration and image haven't changed.
recreate: export EXTRA_PARAMS=--force-recreate
recreate: stop build start

#Services are built once and then tagged.
#If you change a service’s Dockerfile or the contents of its build directory,
#run docker-compose build to rebuild it.
build:
	@${CDC} build

#Starts existing containers for a service.
start:  permission create-log
	@${CDC} up -d ${EXTRA_PARAMS}

#Stops running containers without removing them.
stop:
	@${CDC} down -v

restart: stop start

create-log:
	@if [ ! -d "${LOG_APACHE_DIR}/api" ]; then \
		echo "\033[0;31mLog directory for mamiezi not created yet. Creating...\033[0m"; \
		mkdir ${LOG_APACHE_DIR}/api || echo "\033[0;32mLog directory for api already exists. No need to create.\033[0m" ; \
		sudo chown -R ${USERNAME}:www-data ${LOG_APACHE_DIR}/api; \
	else \
		echo "\033[0;32mLog directory for api already exists. No need to create.\033[0m"; \
	fi

install-db: create-data-dir import-sql

create-data-dir:
	@if [ ! -d "${DATA_PATH}" ]; then \
		mkdir ${DATA_PATH}; \
		sudo chown -R ${USERNAME}:www-data ${DATA_PATH}; \
	else \
		echo  "\033[0;32mData directory already exists.\033[0m"; \
	fi \

import-sql:
	@if [ ! -f "${DATA_PATH}/db.sql" ]; then \
		echo "\033[0;31mMySQL dump not found. Importing mysql dump from server ${HOST}.\033[0m"; \
		scp -r ${HOST}:${HOST_DATA_PATH}/db.sql ${CURRENT_DIR}/db.sql || echo "\033[0;31mFailed to copy db.sql from ${HOST} on ${HOST_DATA_PATH}\033[0m"; \
	else \
	    echo  "\033[0;32mMySQL dump already exists.\033[0m"; \
	fi

init-sql:
	@pv ${DATA_PATH}/db.sql | ${CDC} exec -T mysql mysql -u ${MYSQL_USER} -h localhost -D ${MYSQL_DATABASE} -p${MYSQL_PASSWORD};

reimport-sql:
	@sudo rm -rf ${DATA_PATH}/mysql
	@make import-sql

permission:
	@if ! [ -f .yarn ]; then \
		touch .yarn; \
	fi
	@if ! [ -f .yarnrc ]; then \
		touch .yarnrc; \
	fi
	@if ! [ -d ~/.composer ]; then \
		mkdir ~/.composer; \
	fi
	@if ! [ -f ~/.gitconfig ]; then \
		touch ~/.gitconfig; \
	fi
	@if ! [ -d .cache ]; then \
		mkdir .cache; \
	fi
	@sudo chown -R ${USERNAME}:www-data .yarn .yarnrc ~/.composer ~/.gitconfig .cache ~/.ssh
	@sudo chmod 666 /var/run/docker.sock
